import { h } from "preact";
import { useEffect, useRef, useState } from "preact/hooks";

export const ShowInfoButton = () => {
  const buttonRef = useRef<HTMLButtonElement>(null);
  const modalRef = useRef<HTMLDivElement>(null);
  const [infoIsVisible, setInfoIsVisible] = useState(false);

  useEffect(() => {
    const handleEscape = (e: KeyboardEvent) => {
      if (e.key === "Escape") setInfoIsVisible(false);
    };

    const handleClickOutside = (event: any) => {
      if (
        buttonRef.current &&
        modalRef.current &&
        !modalRef.current.contains(event.target) &&
        !buttonRef.current.contains(event.target)
      ) {
        setInfoIsVisible(false);
      }
    };

    if (infoIsVisible) {
      document.addEventListener("keydown", handleEscape);
      document.addEventListener("mousedown", handleClickOutside);
      document.addEventListener("touchstart", handleClickOutside);
    }

    return () => {
      document.removeEventListener("keydown", handleEscape);
      document.removeEventListener("mousedown", handleClickOutside);
      document.removeEventListener("touchstart", handleClickOutside);
    };
  }, [modalRef, infoIsVisible]);

  return (
    <div>
      {infoIsVisible && (
        <div
          ref={modalRef}
          className="uiButton modal infoModal"
          aria-describedBy="infoButton"
          hidden={!infoIsVisible}
        >
          <p>
            a scifi-inspired study of signed distanced functions and noise fields in
            webgl, by{" "}
            <a
              href="https://novonine.space"
              target="_blank"
              rel="noopener noreferrer"
            >
              novonine
            </a>
            .
          </p>
          <p>
            <a
              href="https://en.wikipedia.org/wiki/Signed_distance_function"
              target="_blank"
              rel="noopener noreferrer"
            >
              signed distance functions
            </a>{" "}
            are fun. With them, you can compute the distance to an object in a metric
            space, provided you have a function to describe that object's volume.
          </p>
          <p>
            when used alongside{" "}
            <a
              href="https://en.wikipedia.org/wiki/Volume_ray_casting"
              target="_blank"
              rel="noopener noreferrer"
            >
              ray marching
            </a>{" "}
            techniques, you can render views of these 3d objects as seen through a 2d
            plane. this project is an experiment in combining this method with
            various noise fields, to manipulate and distort these views.
          </p>
        </div>
      )}

      <button
        ref={buttonRef}
        id="infoButton"
        className="uiButton infoButton"
        aria-label="info"
        aria-expanded={infoIsVisible}
        tabIndex={0}
        onClick={() => setInfoIsVisible(previnfoIsVisible => !previnfoIsVisible)}
      >
        <span role="tooltip">{infoIsVisible ? "x" : "i"}</span>
      </button>
    </div>
  );
};
